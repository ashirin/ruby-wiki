require 'sinatra'
require 'sinatra/activerecord'

ActiveRecord::Base.establish_connection(
	:adapter => 'sqlite3',
	:database => 'wiki.db'
)

class User < ActiveRecord::Base
	validates :username, presence: true, uniqueness: true
	validates :password, presence: true
end

$myinfo = "Aleksandra Chybowska"
@info = ""

def readFile(filename)
	info = ""
	file = File.open(filename)
	
	file.each do |line|
		info = info + line
	end

	file.close
	return info
end

get '/' do
	@info  = readFile("wiki.txt").chomp
	len = @info.length 
	@words = len.to_s

	erb :home
end

get '/about' do
	erb :about
end

get '/register' do
	erb :register
end

post '/register' do
    n = User.new   
    n.username = params[:username]
    n.password = params[:password]    
    
    if n.username == "Admin" and n.password == "Password"
   		n.edit = true 
    end
   	
   	n.save    
   	redirect "/"
end

get '/logout' do
   $credentials = ['','']
   redirect '/'
end

get '/login' do
	erb :login
end

post '/login' do
    $credentials = [params[:username],params[:password]]
    @Users = User.where(:username => $credentials[0]).to_a.first
    
    if @Users && @Users.password == $credentials[1]
		redirect '/'
	else
		$credentials = ['','']
		redirect '/wrongaccount'
	end
end

get '/wrongaccount' do
   erb :wrongaccount
end


get '/edit' do 
	@info = readFile("wiki.txt")
	erb :edit
end

put '/edit' do
	info = "#{params[:message]}"
	@info = info
	file = File.open("wiki.txt", "w")
	file.puts @info
	file.close
	redirect '/'
end

not_found do 
	status 404
	redirect '/'
end
